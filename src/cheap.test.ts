import { Selector } from 'testcafe';
import fs from 'fs';

fixture`Getting Started`
    .page`https://emag.ro`;

class Product {
    type: string;
    name: string;
    price: number;
    link: any;
    match: Array<string>;

    constructor(type:string, name: string, price:number, link:any, match:Array<string> = ['none'] ) {
        this.type = type;
        this.name = name;
        this.price = price;
        this.link = link;
        this.match = ['none']
      }
  }

class ProductTypes {
    type: string;
    keyword: string;
    match: Object;
    total_products: Product[];

    constructor(type:string, keyword:string, match:Object) {
        this.type = type
        this.keyword = keyword
        this.match = match;
        this.total_products = [];
      }
}

class matchProducts {
    // firstType will be the most expensive product
    match_product: ProductTypes;
    // matchProducts will be the other cheaper products to match with
    matching_products: Array<ProductTypes>;
    // matchType will be the string to look for in DOM
    match_criteria: Array<string>
    
    constructor(match_product:ProductTypes, matching_products:Array<ProductTypes>, match_criteria:Array<string>){
        this.match_product = match_product
        this.matching_products = matching_products
        this.match_criteria = match_criteria
    }

};

const sleep = (t: number): Promise<void> => {
    return new Promise((resolve) => {
        setTimeout(() => {resolve(); }, t);
    });
};

function getMatchItems(array:ProductTypes){
    var list_of_items_to_match:Array<string> = []

    for (var match_items of Object.values(array.match)){
        for (var match_values of match_items ){
            if (!(match_values in list_of_items_to_match) && (match_values !== 'none')){
                list_of_items_to_match.push(match_values)
            }
        }
    }
return list_of_items_to_match
}


function appendProductToCategory(product:Product, category:Array<ProductTypes>){
    for (var i = 0; i < category.length; i++ ){
        if (product.type == category[i].type){
            product.match = getMatchItems(category[i])
            category[i].total_products.push(product)
        }
    }
}

  function assertCondition(condition: any, msg?: string): asserts condition {
    if (!condition) {
      throw msg;
    }
  }

//  we will store here all the product category
var items:Array<ProductTypes> = [];

test('CheapSystemBuyTest', async t => { 


    const config = JSON.parse(fs.readFileSync("./configuration.json").toString("utf-8"));

    //  initialise items categories
    for(var i=0; i<(config.components).length; i++){
        
        // filter items with no match category
        if (config.components[i].match == null){
            items.push(new ProductTypes(config.components[i].id, config.components[i].keywords[0], {none:['none']}));
        } else {
            items.push(new ProductTypes(config.components[i].id, config.components[i].keywords[0], config.components[i].match[0]));
        }
    }

    for (const c of config.components) {
        for (const key of c.keywords){
            await t.navigateTo(`${config.url}/search/${c.id}/${key}/c`);
            
            // set constants for variables
            const price = Selector(".product-new-price");
            const name = Selector(".pad-hrz-xs > a");
            const link = Selector(".card-v2-info > a:first-child");
            
            // count the number of items for each category
            var count = await link.count;

            for (var i = 0; i < count; i++ ){

                // append products with no match carachteristics
                var product = new Product(c.id,
                                        await name.nth(i).innerText, 
                                        parseFloat((await price.nth(i).innerText).replace(" Lei", "").replace("de la","").replace(".","")) , 
                                        await link.nth(i).getAttribute("href"));

                appendProductToCategory(product,items)
            }

        }
    }

    
    // 1. Find the cheapest product  
    for (var item of items){
        item.total_products.sort(function(a:any, b:any){return a.price - b.price});
    }
    // 2. Check for match

    // 2.1. Find products types that require a match
    var products_to_match:Array<ProductTypes> = [];
    var products_in_cart:Array<Product> = [];
    for (var i = 0; i < items.length; i++ ){
        if ((Object.values(items[i].match)[0])[0] !== 'none' ){
            products_to_match.push(items[i])
        } else {
            products_in_cart.push(items[i].total_products[0])
        }
    }

    // 2.2 Find the most expensive product (if we compare the first value from all total matches items we get the most expensiv product)
    products_to_match.sort(function(a:any, b:any){return b.total_products[0].price - a.total_products[0].price})
    const most_expensive = products_to_match[0]
    const other_items_to_match = products_to_match.slice(1)

    // 2.3 Set the searching order - start from the most expensive product
    var matching: matchProducts = new matchProducts(most_expensive,
                                                    other_items_to_match,
                                                    //  This item can be one string 
                                                    most_expensive.total_products[0].match)

    console.log(`Searching to match ${matching.match_product.type} with others by ${matching.match_criteria}`)
    products_in_cart.push(matching.match_product.total_products[0])

    // 2.2 Get the match type from the link for most expensive product
    var match_for_other_products;

    // 2.2.1 Iterate over the maching criterias
    for (var match_to_look_for of matching.match_criteria){

        // 2.2.2 Go to the ceapest product in the most expensive category
        await t.navigateTo(matching.match_product.total_products[0].link);

        match_for_other_products  = await Selector(".specifications-table").find("td").withText(`${match_to_look_for}`).nextSibling().innerText
        console.log(`Start looking for other parts that have ${match_to_look_for} of ${match_for_other_products}`)

        var matched_product;
        // 2.2.3 Start iterating over the other category (less expensive ones) to look for a match
        for (var category_to_match of matching.matching_products){
            console.log(`Looking in ${category_to_match.type} for ${match_to_look_for} of ${match_for_other_products}`)

            // 2.3 Start iterating over the total_products in every category in ascending order (cheap to expensive)
            for (var products_matching of category_to_match.total_products){
                console.log(`Checking if ${products_matching.name} has ${match_to_look_for} of ${match_for_other_products} `)

                await t.navigateTo(products_matching.link);

                var match_criteria_of_product_that_matches = await Selector(".specifications-table").find("td").withText(`${match_to_look_for}`).nextSibling().innerText
                if (match_criteria_of_product_that_matches === match_for_other_products){
                    var name_of_product_that_matches = products_matching.name;
                    var price_of_product_that_matches = products_matching.price;
                    products_in_cart.push(products_matching)

                    console.log(`We found ${name_of_product_that_matches} that has ${match_to_look_for} of ${match_criteria_of_product_that_matches} at cheapest price ${price_of_product_that_matches} \n`);
                    break;
                }
            }
        }

    // 3 Check if total price fits the budget and add to cart
    const sum = products_in_cart.reduce((a, b) => a + b.price, 0);

    assertCondition(sum < config.expectedTotalLimit, "Budget exceeded")
    assertCondition(items.length === products_in_cart.length, "Cart not completed")

    if ((sum < config.expectedTotalLimit) && (items.length === products_in_cart.length)){
        for (var produs of products_in_cart){
            await t.navigateTo(produs.link);
            await sleep(500)
            const button = Selector(".product-cart-fav-buttons").nth(0);
            await t.click(button);
        }

        console.log(`Final cart of ${sum} LEI completed with following products: `)
        products_in_cart.forEach(product => console.log(product.name))
        
    } 
    }
});
