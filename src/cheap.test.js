"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var testcafe_1 = require("testcafe");
var fs_1 = __importDefault(require("fs"));
fixture(templateObject_1 || (templateObject_1 = __makeTemplateObject(["Getting Started"], ["Getting Started"]))).page(templateObject_2 || (templateObject_2 = __makeTemplateObject(["https://emag.ro"], ["https://emag.ro"])));
var Product = /** @class */ (function () {
    function Product(type, name, price, link, match) {
        if (match === void 0) { match = ['none']; }
        this.type = type;
        this.name = name;
        this.price = price;
        this.link = link;
        this.match = ['none'];
    }
    return Product;
}());
var ProductTypes = /** @class */ (function () {
    function ProductTypes(type, keyword, match) {
        this.type = type;
        this.keyword = keyword;
        this.match = match;
        this.total_products = [];
    }
    return ProductTypes;
}());
var matchProducts = /** @class */ (function () {
    function matchProducts(match_product, matching_products, match_criteria) {
        this.match_product = match_product;
        this.matching_products = matching_products;
        this.match_criteria = match_criteria;
    }
    return matchProducts;
}());
;
var sleep = function (t) {
    return new Promise(function (resolve) {
        setTimeout(function () { resolve(); }, t);
    });
};
function getMatchItems(array) {
    var list_of_items_to_match = [];
    for (var _i = 0, _a = Object.values(array.match); _i < _a.length; _i++) {
        var match_items = _a[_i];
        for (var _b = 0, match_items_1 = match_items; _b < match_items_1.length; _b++) {
            var match_values = match_items_1[_b];
            if (!(match_values in list_of_items_to_match) && (match_values !== 'none')) {
                list_of_items_to_match.push(match_values);
            }
        }
    }
    return list_of_items_to_match;
}
function appendProductToCategory(product, category) {
    for (var i = 0; i < category.length; i++) {
        if (product.type == category[i].type) {
            product.match = getMatchItems(category[i]);
            category[i].total_products.push(product);
        }
    }
}
function assertCondition(condition, msg) {
    if (!condition) {
        throw msg;
    }
}
//  we will store here all the product category
var items = [];
test('CheapSystemBuyTest', function (t) { return __awaiter(void 0, void 0, void 0, function () {
    var config, i, _i, _a, c, _b, _c, key, price, name_1, link, count, i, product, _d, _e, _f, _g, items_1, item, products_to_match, products_in_cart, i, most_expensive, other_items_to_match, matching, match_for_other_products, _h, _j, match_to_look_for, matched_product, _k, _l, category_to_match, _m, _o, products_matching, match_criteria_of_product_that_matches, name_of_product_that_matches, price_of_product_that_matches, sum, _p, products_in_cart_1, produs, button;
    return __generator(this, function (_q) {
        switch (_q.label) {
            case 0:
                config = JSON.parse(fs_1.default.readFileSync("./configuration.json").toString("utf-8"));
                //  initialise items categories
                for (i = 0; i < (config.components).length; i++) {
                    // console.log(typeof(config.components[i].match))
                    if (config.components[i].match == null) {
                        items.push(new ProductTypes(config.components[i].id, config.components[i].keywords[0], { none: ['none'] }));
                    }
                    else {
                        items.push(new ProductTypes(config.components[i].id, config.components[i].keywords[0], config.components[i].match[0]));
                    }
                }
                console.log(items);
                _i = 0, _a = config.components;
                _q.label = 1;
            case 1:
                if (!(_i < _a.length)) return [3 /*break*/, 12];
                c = _a[_i];
                _b = 0, _c = c.keywords;
                _q.label = 2;
            case 2:
                if (!(_b < _c.length)) return [3 /*break*/, 11];
                key = _c[_b];
                return [4 /*yield*/, t.navigateTo(config.url + "/search/" + c.id + "/" + key + "/c")];
            case 3:
                _q.sent();
                price = testcafe_1.Selector(".product-new-price");
                name_1 = testcafe_1.Selector(".pad-hrz-xs > a");
                link = testcafe_1.Selector(".card-v2-info > a:first-child");
                return [4 /*yield*/, link.count];
            case 4:
                count = _q.sent();
                i = 0;
                _q.label = 5;
            case 5:
                if (!(i < count)) return [3 /*break*/, 10];
                _d = Product.bind;
                _e = [void 0, c.id];
                return [4 /*yield*/, name_1.nth(i).innerText];
            case 6:
                _e = _e.concat([_q.sent()]);
                _f = parseFloat;
                return [4 /*yield*/, price.nth(i).innerText];
            case 7:
                _e = _e.concat([_f.apply(void 0, [(_q.sent()).replace(" Lei", "").replace("de la", "").replace(".", "")])]);
                return [4 /*yield*/, link.nth(i).getAttribute("href")];
            case 8:
                product = new (_d.apply(Product, _e.concat([_q.sent()])))();
                appendProductToCategory(product, items);
                _q.label = 9;
            case 9:
                i++;
                return [3 /*break*/, 5];
            case 10:
                _b++;
                return [3 /*break*/, 2];
            case 11:
                _i++;
                return [3 /*break*/, 1];
            case 12:
                // 1. Find the cheapest product  
                for (_g = 0, items_1 = items; _g < items_1.length; _g++) {
                    item = items_1[_g];
                    item.total_products.sort(function (a, b) { return a.price - b.price; });
                }
                products_to_match = [];
                products_in_cart = [];
                for (i = 0; i < items.length; i++) {
                    if ((Object.values(items[i].match)[0])[0] !== 'none') {
                        products_to_match.push(items[i]);
                    }
                    else {
                        products_in_cart.push(items[i].total_products[0]);
                    }
                }
                // 2.2 Find the most expensive product (if we compare the first value from all total matches items we get the most expensiv product)
                products_to_match.sort(function (a, b) { return b.total_products[0].price - a.total_products[0].price; });
                most_expensive = products_to_match[0];
                other_items_to_match = products_to_match.slice(1);
                matching = new matchProducts(most_expensive, other_items_to_match, 
                //  This item can be one string 
                most_expensive.total_products[0].match);
                console.log("Searching to match " + matching.match_product.type + " with others by " + matching.match_criteria);
                products_in_cart.push(matching.match_product.total_products[0]);
                _h = 0, _j = matching.match_criteria;
                _q.label = 13;
            case 13:
                if (!(_h < _j.length)) return [3 /*break*/, 30];
                match_to_look_for = _j[_h];
                // 2.2.2 Go to the ceapest product in the most expensive category
                return [4 /*yield*/, t.navigateTo(matching.match_product.total_products[0].link)];
            case 14:
                // 2.2.2 Go to the ceapest product in the most expensive category
                _q.sent();
                return [4 /*yield*/, testcafe_1.Selector(".specifications-table").find("td").withText("" + match_to_look_for).nextSibling().innerText];
            case 15:
                match_for_other_products = _q.sent();
                console.log("Start looking for other parts that have " + match_to_look_for + " of " + match_for_other_products);
                _k = 0, _l = matching.matching_products;
                _q.label = 16;
            case 16:
                if (!(_k < _l.length)) return [3 /*break*/, 22];
                category_to_match = _l[_k];
                console.log("Looking in " + category_to_match.type + " for " + match_to_look_for + " of " + match_for_other_products);
                _m = 0, _o = category_to_match.total_products;
                _q.label = 17;
            case 17:
                if (!(_m < _o.length)) return [3 /*break*/, 21];
                products_matching = _o[_m];
                console.log("Checking if " + products_matching.name + " has " + match_to_look_for + " of " + match_for_other_products + " ");
                return [4 /*yield*/, t.navigateTo(products_matching.link)];
            case 18:
                _q.sent();
                return [4 /*yield*/, testcafe_1.Selector(".specifications-table").find("td").withText("" + match_to_look_for).nextSibling().innerText];
            case 19:
                match_criteria_of_product_that_matches = _q.sent();
                if (match_criteria_of_product_that_matches === match_for_other_products) {
                    name_of_product_that_matches = products_matching.name;
                    price_of_product_that_matches = products_matching.price;
                    products_in_cart.push(products_matching);
                    console.log("We found " + name_of_product_that_matches + " that has " + match_to_look_for + " of " + match_criteria_of_product_that_matches + " at cheapest price " + price_of_product_that_matches + " \n");
                    return [3 /*break*/, 21];
                }
                _q.label = 20;
            case 20:
                _m++;
                return [3 /*break*/, 17];
            case 21:
                _k++;
                return [3 /*break*/, 16];
            case 22:
                sum = products_in_cart.reduce(function (a, b) { return a + b.price; }, 0);
                assertCondition(sum < config.expectedTotalLimit, "Budget exceeded");
                assertCondition(items.length === products_in_cart.length, "Cart not completed");
                if (!((sum < config.expectedTotalLimit) && (items.length === products_in_cart.length))) return [3 /*break*/, 29];
                _p = 0, products_in_cart_1 = products_in_cart;
                _q.label = 23;
            case 23:
                if (!(_p < products_in_cart_1.length)) return [3 /*break*/, 28];
                produs = products_in_cart_1[_p];
                return [4 /*yield*/, t.navigateTo(produs.link)];
            case 24:
                _q.sent();
                return [4 /*yield*/, sleep(500)];
            case 25:
                _q.sent();
                button = testcafe_1.Selector(".product-cart-fav-buttons").nth(0);
                return [4 /*yield*/, t.click(button)];
            case 26:
                _q.sent();
                _q.label = 27;
            case 27:
                _p++;
                return [3 /*break*/, 23];
            case 28:
                console.log("Final cart of " + sum + " LEI completed with following products: ");
                products_in_cart.forEach(function (product) { return console.log(product.name); });
                _q.label = 29;
            case 29:
                _h++;
                return [3 /*break*/, 13];
            case 30: return [2 /*return*/];
        }
    });
}); });
var templateObject_1, templateObject_2;
